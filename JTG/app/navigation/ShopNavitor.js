import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ProductInfo from "../screens/ProductInfo";
import Home from "../screens/Home";
import MyCart from "../screens/MyCart";

const Stack = createStackNavigator();
const ShopNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Home" component={Home} />
    <Stack.Screen name="ProductInfo" component={ProductInfo} />
    <Stack.Screen name="MyCart" component={MyCart} />
  </Stack.Navigator>
);

export default ShopNavigator;
