import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import AppCamera from "../components/AppCamera";
import ListingEditScreen from "../screens/ListingEditScreen";

const Stack = createStackNavigator();
const ListImagesNavigator = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="listEditScreen" component={ListingEditScreen} />
    <Stack.Screen name="appCamera" component={AppCamera} />
  </Stack.Navigator>
);

export default ListImagesNavigator;
