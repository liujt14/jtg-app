import react from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import AccountNavigator from "./AccountNavigator";
import MapNavigator from "./MapNavigator";
import CommunityNavigator from "./CommunityNavitor";
import ShopNavigator from "./ShopNavitor";
import CameraNavigator from "./CameraNavigator";
import ListingEditScreen from "../screens/ListingEditScreen";
import NewListingButton from "./NewListingButton";
import ListImagesNavigator from "./ListImagesNavigator";

const Tab = createBottomTabNavigator();

const AppNavigatorFive = () => (
  //   <Tab.Navigator>
  //     <Tab.Screen
  //       name="Feed"
  //       component={FeedNavigator}
  //       options={{
  //         tabBarIcon: ({ color, size }) => (
  //           <MaterialCommunityIcons name="home" color={color} size={size} />
  //         ),
  //       }}
  //     />
  //     <Tab.Screen
  //       name="ListingEdit"
  //       component={ListingEditScreen}
  //       options={({ navigation }) => ({
  //         tabBarButton: () => (
  //           <NewListingButton
  //             onPress={() => navigation.navigate("ListingEdit")}
  //           />
  //         ),
  //         headerShown: false,
  //         tabBarIcon: ({ color, size }) => (
  //           <MaterialCommunityIcons
  //             name="plus-circle"
  //             color={color}
  //             size={size}
  //           />
  //         ),
  //       })}
  //     />
  //     <Tab.Screen
  //       name="AccountScreen"
  //       component={AccountNavigator}
  //       options={{
  //         tabBarIcon: ({ color, size }) => (
  //           <MaterialCommunityIcons name="account" color={color} size={size} />
  //         ),
  //       }}
  //     />
  //   </Tab.Navigator>
  <Tab.Navigator
    screenOptions={
      {
        //tabBarActiveBackgroundColor: "gray",
        //tabBaractiveTintColor: "white",
      }
    }
  >
    <Tab.Screen
      name="約拍"
      component={MapNavigator}
      options={{
        tabBarIcon: ({ size, color }) => (
          <MaterialCommunityIcons name="map-marker" size={size} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="論壇"
      component={CommunityNavigator}
      options={{
        tabBarIcon: ({ size, color }) => (
          <MaterialCommunityIcons name="chat" size={size} color={color} />
        ),
      }}
    />
    <Tab.Screen
      name="相機"
      component={ListImagesNavigator}
      options={{
        headerShown: false,
        tabBarIcon: ({ size, color }) => (
          <MaterialCommunityIcons
            name="camera-enhance"
            size={size}
            color={color}
          />
        ),
      }}
    />
    {/* <Tab.Screen
      name="相機"
      component={ListImagesNavigator}
      options={() => ({
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons
            name="camera-enhance"
            color={color}
            size={size}
          />
        ),
      })}
    /> */}
    <Tab.Screen
      name="商店"
      component={ShopNavigator}
      options={{
        tabBarIcon: ({ size, color }) => (
          <MaterialCommunityIcons name="shopping" size={size} color={color} />
        ),
      }}
    />

    <Tab.Screen
      name="账户"
      component={AccountNavigator}
      options={{
        tabBarIcon: ({ size, color }) => (
          <MaterialCommunityIcons name="account" size={size} color={color} />
        ),
      }}
    />
  </Tab.Navigator>
);

export default AppNavigatorFive;
