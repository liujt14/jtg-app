import React from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function Login(props) {
  return (
    <SafeAreaView style={styles.container}>
      <Image
        style={styles.image}
        source={require("../assets/adaptive-icon.png")}
      ></Image>
      <View
        style={{
          backgroundColor: "gold",
          width: 80,
          height: 80,
          position: "absolute",
          top: 34,
          left: 10,
        }}
      ></View>
      <View
        style={{
          backgroundColor: "orange",
          width: 80,
          height: 80,
          position: "absolute",
          top: 34,
          right: 10,
        }}
      ></View>
    </SafeAreaView>
  );
}

export default Login;
