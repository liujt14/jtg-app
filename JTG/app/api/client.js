import { create } from "apisauce";
import authStorage from "../auth/storage";

import cache from "../utility/cache";

const apiClient = create({
  baseURL: "http://127.0.0.1:9000/api",
});

apiClient.addAsyncRequestTransform(async (request) => {
  const authToken = await authStorage.getToken();
  if (!authToken) return;
  request.headers["x-auth-token"] = authToken;
});

const get = apiClient.get;
apiClient.get = async (url, params, axiosConfig) => {
  // before

  const response = await get(url, params, axiosConfig);
  if (response.ok) {
    cache.store(url, response.data);
    return response;
  }

  // after
  const data = await cache.get(rul);
  return data ? { ok: true, data } : response;
};

export default apiClient;
