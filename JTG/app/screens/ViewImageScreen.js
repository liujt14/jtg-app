import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

function ViewImageScreen(props) {
  return (
    <View style={styles.imageContainer}>
      <View style={styles.closeIcon}>
        <MaterialCommunityIcons name="close" color="white" size={30} />
      </View>
      <Image
        resizeMode="contain"
        style={styles.image}
        source={require("../assets/DJI_0183.jpg")}
      />
      <View style={styles.saveIcon}>
        <MaterialCommunityIcons
          name="trash-can-outline"
          color="white"
          size={30}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  imageContainer: {
    flex: 1,
    backgroundColor: "#000",
  },
  image: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  closeIcon: {
    position: "absolute",
    top: 40,
    left: 10,
  },
  saveIcon: {
    position: "absolute",
    top: 40,
    right: 10,
  },
});

export default ViewImageScreen;
