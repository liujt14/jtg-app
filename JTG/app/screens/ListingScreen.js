import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet } from "react-native";

import routes from "../navigation/routes";
import Screen from "../components/lists/Screen";
import listingsApi from "../api/listings";
import Card from "../components/lists/Card";
import colors from "../config/colors";
import AppText from "../components/lists/AppText";
import AppButton from "../components/lists/AppButton";
import ActivityIndicator from "../components/ActivityIndicator";
import useApi from "../hooks/useApi";

// const listings = [
//   {
//     id: 1,
//     title: "Red jacket for sale",
//     price: "$100",
//     image: require("../assets/wechat.png"),
//   },
//   {
//     id: 2,
//     title: "Apple for sale",
//     price: "$1000",
//     image: require("../assets/facebook.png"),
//   },
// ];
function ListingScreen({ navigation }) {
  const getListingsApi = useApi(listingsApi.getListings); //deconstructuring
  useEffect(() => {
    getListingsApi.request();
  }, []);

  return (
    <>
      <ActivityIndicator visible={getListingsApi.loading} />
      <Screen style={styles.screen}>
        {getListingsApi.error && (
          <>
            <AppText>
              Counldn't retrieve the listings data
              <AppButton title="Retry" onPress={getListingsApi.request} />
            </AppText>
          </>
        )}

        <FlatList
          data={getListingsApi.data}
          keyExtractor={(listing) => listing.id.toString()}
          renderItem={({ item }) => (
            <Card
              title={item.title}
              subTitle={item.price}
              imageUrl={item.images[0].url}
              onPress={() => navigation.navigate(routes.LISTING_DETAILS, item)}
            />
          )}
        />
      </Screen>
    </>
  );
}

const styles = StyleSheet.create({
  screen: {
    padding: 20,
    backgroundColor: colors.light,
  },
});
export default ListingScreen;
