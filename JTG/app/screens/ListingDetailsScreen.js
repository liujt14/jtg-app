import React from "react";
import { View, Image, StyleSheet, Text } from "react-native";
import ListItem from "../components/ListItem";
import { AppButton } from "../components/lists";

function ListingDetailsScreen({ route }) {
  const listing = route.params;
  const handlepress = () => {
    console.log(listing);
  };
  return (
    <View>
      <Image style={styles.image} source={{ url: listing.images[0].url }} />
      <View style={styles.detailsContainer}>
        <Text style={styles.title}> {listing.title}</Text>
        <Text style={styles.subTitle}> ${listing.price}</Text>
        <View style={styles.userContainer}>
          <ListItem
            image={require("../components/database/images/products/boat1.png")}
            title="Steve"
            subTitle="5 Listings"
          />
          <AppButton title="Contact me" onPress={handlepress} />
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 300,
  },
  detailsContainer: {
    padding: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: "500",
  },
  subTitle: {
    fontStyle: "italic",
    fontWeight: "bold",
    color: "green",
    marginVertical: 10,
  },
  userContainer: {
    marginVertical: 40,
  },
});

export default ListingDetailsScreen;
