import React from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Image,
  Text,
  Button,
} from "react-native";

import { AppButton } from "../components/lists";

function WelcomeScreen({ navigation }) {
  return (
    <ImageBackground
      style={styles.background}
      source={require("../assets/welcome.jpg")}
    >
      <View style={styles.logoContainer}>
        <Image
          style={styles.logo}
          source={require("../assets/icon-small.png")}
        />
        <Text style={styles.text}> JTG Portrait Photography </Text>
      </View>
      <View>
        <View style={styles.loginButton}>
          <AppButton
            title="Login"
            onPress={() => navigation.navigate("Login")}
          ></AppButton>
        </View>
        <View style={styles.registerButton}>
          <AppButton
            title="Register"
            onPress={() => navigation.navigate("Register")}
          ></AppButton>
        </View>
      </View>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  buttonsContainer: {
    width: "90%",
    height: 70,
    backgroundColor: "#fc5c65",
    marginBottom: 50,
  },
  loginButton: {
    width: 150,
    height: 60,
    marginBottom: 0,
  },
  registerButton: {
    width: 150,
    height: 50,
    marginBottom: 50,
  },
  logo: {
    width: 50,
    height: 50,
    margin: 10,
  },
  logoContainer: {
    position: "absolute",
    top: 60,
    alignItems: "center",
  },
  text: {
    fontFamily: "American Typewriter",
    fontSize: 20,
    fontWeight: "400",
    textShadowColor: "white",
    textShadowRadius: 10,
    textShadowOffset: { width: 1, height: 1 },
  },
});
export default WelcomeScreen;
