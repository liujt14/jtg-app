import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";

import ListItem from "../components/ListItem";
import ListItemDeleteAction from "../components/ListItemDeleteAction";
import ListSeparator from "../components/ListSeparator";
import Screen from "../components/lists/Screen";
import colors from "../config/colors";

const initialMessages = [
  {
    id: 1,
    title: "商店",
    description: "Hi, are you still interested in our services?",
    image: require("../assets/welcome.jpg"),
  },
  {
    id: 2,
    title: "朋友",
    description: "你好，请问什么时候有空呢？",
    image: require("../assets/thegirl2.jpg"),
  },
];

function MessagesScreen(props) {
  const [messages, setMessage] = useState(initialMessages);
  const [refreshing, setRefreshing] = useState(false);

  const handleDelete = (message) => {
    setMessage(messages.filter((m) => m.id !== message.id));
  };
  return (
    <Screen>
      <FlatList
        data={messages}
        keyExtractor={(message) => message.id.toString()}
        ItemSeparatorComponent={ListSeparator}
        renderItem={({ item }) => (
          <ListItem
            title={item.title}
            subTitle={item.description}
            image={item.image}
            onPress={() => console.log("Message selected", item)}
            renderRightActions={() => (
              <ListItemDeleteAction onPress={() => handleDelete(item)} />
            )}
            // renderRightActions={() => (
            //   <View style={{ backgroundColor: "green", width: 70 }}></View>
            // )}
          />
        )}
        refreshing={refreshing}
        onRefresh={() => {
          setMessage([
            {
              id: 2,
              title: "T2",
              description: "D2",
              image: require("../assets/google.png"),
            },
          ]);
        }}
      />
    </Screen>
  );
}
const styles = StyleSheet.create({});

export default MessagesScreen;
