import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  Dimensions,
} from "react-native";
import {
  MaterialCommunityIcons,
  Fontisto,
  MaterialIcons,
  Ionicons,
} from "@expo/vector-icons";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import { AppButton } from "../components/lists";
import useAuth from "../auth/useAuth";

var { width } = Dimensions.get("window");

function LoginScreen(props) {
  const auth = useAuth();
  const [LoginFailed, SetLoginFailed] = useState(false);

  const handleLogin = async ({ email, password }) => {
    const result = await authApi.login(email, password);
    if (!result.ok) return SetLoginFailed(true);
    SetLoginFailed(false);
    auth.logIn(result.data);
  };

  return (
    /*<ImageBackground
      style={styles.background}
      source={require("../assets/splash.png")}
    >*/
    <View style={styles.background}>
      <Image style={styles.logo} source={require("../assets/Logo.png")} />

      <TextInput
        placeholder="請輸入電郵"
        clearButtonMode="while-editing"
        style={styles.textInputStyle}
      ></TextInput>
      <TextInput
        placeholder="請輸入密碼"
        clearButtonMode="while-editing"
        sourceTextEntry={true}
        selectionColor={"black"}
        keyboardAppearance={"dark"}
        style={styles.textInputStyle}
      ></TextInput>
      <View style={styles.selectionOptions}>
        <View style={styles.options}>
          <BouncyCheckbox
            size={15}
            text="想Keep住登入狀態"
            textStyle={{
              textDecorationLine: "none",
              fontSize: 13,
            }}
            onPress={(isChecked) => {}}
          />
        </View>
        <View style={styles.options}>
          <BouncyCheckbox
            size={15}
            text="使用Touch/Face ID"
            textStyle={{
              textDecorationLine: "none",
              fontSize: 13,
            }}
            onPress={(isChecked) => {}}
          />
        </View>
      </View>
      <Text style={styles.fogotPs}>唔記得左密碼？</Text>
      <View style={styles.settingStyle}>
        <View style={styles.register}>
          <Text style={{ color: "white", fontSize: 17, fontWeight: "800" }}>
            立即註冊
          </Text>
        </View>
        <View style={styles.loginButton}>
          <AppButton title="登錄" onPress={handleLogin} />
        </View>
      </View>
      <View style={styles.otherLoginStyles}>
        <Text style={{ color: "#6e6969" }}>其他登錄方式: </Text>
        <View
          style={{
            borderBottomWidth: 0.5,
            marginTop: 10,
            borderBottomColor: "gray",
          }}
        />
        <View style={styles.otherOptions}>
          <View style={styles.otherOption}>
            <MaterialIcons name="phone-android" size={35} color="black" />
          </View>
          <View style={styles.otherOption}>
            <MaterialCommunityIcons name="email" size={35} color="black" />
          </View>
          <View style={styles.otherOption}>
            <Fontisto name="apple" size={35} color="black" />
          </View>
          <View style={styles.otherOption}>
            <MaterialIcons name="facebook" size={35} color="black" />
          </View>
          <View style={styles.otherOption}>
            <Ionicons name="logo-google" size={35} color="black" />
          </View>
        </View>
      </View>
    </View>
    /*</ImageBackground>*/
  );
}
const styles = StyleSheet.create({
  background: {
    height: "100%",
    alignItems: "center",
    backgroundColor: "white",
  },
  logoContainer: {
    position: "absolute",
    top: 40,
    alignItems: "flex-end",
  },
  logo: {
    width: 150,
    height: 75,
    marginTop: 80,
    marginBottom: 30,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: "yellow",
    alignSelf: "flex-start",
    position: "relative",
    left: 20,
  },
  textInputStyle: {
    height: 38,
    width: width * 0.8,
    backgroundColor: "white",
    marginBottom: 1,
    paddingLeft: 10,
    textAlign: "left",
    borderBottomColor: "darkgray", // Add this to specify bottom border color
    borderBottomWidth: 0.6,
  },
  selectionOptions: {
    width: width * 0.8,
    height: 40,
    alignItems: "flex-start",
    padding: 3,
  },
  options: {
    margin: 3,
    marginTop: 15,
  },
  loginButton: {
    width: width * 0.5,
    height: 40,
    marginTop: 30,
    marginBottom: 20,
    paddingLeft: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 20,
    alignSelf: "flex-end",
  },
  settingStyle: {
    flexDirection: "column",
    width: width * 0.9,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  fogotPs: {
    textDecorationLine: "underline",
    color: "gray",
    alignSelf: "flex-start",
    marginTop: 100,
    marginLeft: width * 0.12,
  },
  fogotPsView: {
    width: width * 0.4,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 6,
  },
  register: {
    width: width * 0.3,
    height: 50,
    backgroundColor: "sandybrown",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "flex-end",
    borderRadius: 25,
    marginTop: 60,
  },
  otherLoginStyles: {
    width: width * 0.9,
    flexDirection: "column",
    position: "absolute",
    bottom: 40,
  },

  otherOptions: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
  },
  otherOption: {
    flex: 1,
  },
});
export default LoginScreen;
