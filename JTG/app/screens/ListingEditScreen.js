import React, { useState } from "react";
import {
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import * as Yup from "yup";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import colors from "../config/colors";

import CategoryPickerItem from "../components/lists/CategoryPickerItem";
import {
  AppForm,
  AppFormField,
  AppFormPicker,
  SubmitButton,
} from "../components/forms";
import Screen from "../components/lists/Screen";
import FormImagePicker from "../components/forms/FormImagePicker";
import listingsApi from "../api/listings";
import useLocation from "../hooks/useLocation";
import UploadScreen from "./UploadScreen";
import { AppButton } from "../components/lists";

const validationSchema = Yup.object().shape({
  title: Yup.string().required().min(1).label("Title"),
  price: Yup.string().required().min(1).max(10000).label("Price"),
  description: Yup.string().label("Description"),
  category: Yup.object().required().nullable().label("Category"),
  images: Yup.array().min(1, "Please select at least one image."),
});
const categories = [
  {
    label: "人像",
    value: 1,
    backgroundColor: "pink",
    icon: "account-circle",
  },
  {
    label: "证件照",
    value: 2,
    backgroundColor: "lightgreen",
    icon: "card-account-details",
  },
  {
    label: "群体照",
    value: 3,
    backgroundColor: "lightblue",
    icon: "account-group",
  },
  {
    label: "街头户外",
    value: 4,
    backgroundColor: "orange",
    icon: "routes-clock",
  },
  {
    label: "婚礼摄影",
    value: 5,
    backgroundColor: "chocolate",
    icon: "necklace",
  },
  {
    label: "黑白摄影",
    value: 6,
    backgroundColor: "gray",
    icon: "invert-colors",
  },
];
function ListingEditScreen({ navigation }) {
  const location = useLocation();
  const [uploadVisible, setUploadVisible] = useState(false);
  const [progress, setProgress] = useState(0);

  const handleSubmit = async (listing, { resetForm }) => {
    setProgress(0);
    setUploadVisible(true);
    const result = await listingsApi.addListing(
      { ...listing, location },
      (progress) => setProgress(progress)
    );

    if (!result.ok) {
      setUploadVisible(false);
      return alert("Could not save the listing.");
    }
    resetForm();
  };

  return (
    <Screen style={styles.container}>
      <UploadScreen
        onDone={() => setUploadVisible(false)}
        progress={progress}
        visible={uploadVisible}
      />
      <AppForm
        initialValues={{
          title: "",
          price: "",
          description: "",
          category: null,
          images: [],
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
      >
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("appCamera");
          }}
        >
          <View style={styles.cameraContainer}>
            <MaterialCommunityIcons
              color={colors.medium}
              name="camera"
              size={40}
            />
          </View>
        </TouchableOpacity>

        <FormImagePicker name="images" />
        <AppFormField maxLength={255} name="title" placeholder="Title" />

        <AppFormField
          keyboardType="numeric"
          maxLength={8}
          name="price"
          placeholder="Price"
          width={120}
        />
        <AppFormPicker
          PickerItemComponent={CategoryPickerItem}
          items={categories}
          name="category"
          numberOfColumns={3}
          placeholder="Category"
          width="50%"
        />
        <AppFormField
          maxLength={255}
          multiline
          name="description"
          numberOfLines={3}
          placeholder="Description"
        />
        <SubmitButton title="Post" />
      </AppForm>
    </Screen>
  );
}
const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  cameraContainer: {
    alignItems: "center",
    backgroundColor: colors.light,
    borderRadius: 15,
    height: 100,
    justifyContent: "center",
    overflow: "hidden",
    width: 100,
    marginBottom: 10,
  },
});

export default ListingEditScreen;
