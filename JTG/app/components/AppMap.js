import * as React from "react";
import MapView, { Marker, Callout, Circle } from "react-native-maps";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ShadowPropTypesIOS,
} from "react-native";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import colors from "../config/colors";
import { AppButton } from "../components/lists";

const GOOGLE_MAPS_APIKEY = "AIzaSyCVNbs76Gr3Nkcx_cHrFV086Z_oITGb7tU";
const initialRegion = {
  latitude: 22.419469,
  longitude: 114.205788,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
};
const shops = {
  shatin: {
    latitude: 22.382210307207416,
    longitude: 114.1869535082919,
  },
  hospital: {
    latitude: 22.379811373112126,
    longitude: 114.20153461728155,
  },
  hengon: {
    latitude: 22.417623771608287,
    longitude: 114.22575358298144,
  },
};

function AppMap() {
  const [pin, setPin] = React.useState(initialRegion);
  const [searchRegion, setSearchRegion] = React.useState(initialRegion);

  const dragStart = (event) => {
    console.log("drag start", event.nativeEvent.coordinate);
  };
  const dragEnd = (event) => {
    setPin({
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude,
    });
  };
  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <Text>Origin</Text>
        <GooglePlacesAutocomplete
          placeholder="Search"
          fetchDetails
          GooglePlacesSearchQuery={{
            rankby: "distance",
          }}
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true

            setSearchRegion({
              latitude: details.geometry.location.lat,
              longitude: details.geometry.location.lng,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            });
          }}
          query={{
            key: GOOGLE_MAPS_APIKEY,
            language: "en",
            components: "country:hk",
            type: "establishment",
            radius: 30000,
            location: `${searchRegion.latitude}, ${searchRegion.longitude}`,
          }}
          styles={{
            textInput: styles.input,
            listView: {
              backgroundColor: "white",
            },
          }}
        />
        <Text>Destination</Text>
        <GooglePlacesAutocomplete
          placeholder="Search"
          fetchDetails
          GooglePlacesSearchQuery={{
            rankby: "distance",
          }}
          onPress={(data, details = null) => {
            // 'details' is provided when fetchDetails = true

            setSearchRegion({
              latitude: details.geometry.location.lat,
              longitude: details.geometry.location.lng,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            });
          }}
          query={{
            key: GOOGLE_MAPS_APIKEY,
            language: "en",
            components: "country:hk",
            type: "establishment",
            radius: 30000,
            location: `${searchRegion.latitude}, ${searchRegion.longitude}`,
          }}
          styles={{
            textInput: styles.input,
            listView: {
              backgroundColor: "white",
            },
          }}
        />
        <AppButton title="Go" />
      </View>
      <MapView
        style={styles.map}
        initialRegion={{
          latitude: 22.419469,
          longitude: 114.205788,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
        provider="google"
        showsUserLocation
      >
        <Marker
          coordinate={{
            latitude: searchRegion.latitude,
            longitude: searchRegion.longitude,
          }}
        />
        <Marker
          pinColor="red"
          coordinate={pin}
          draggable
          onDragStart={dragStart}
          onDragEnd={dragEnd}
        >
          <Callout>
            <Text>I'm here</Text>
          </Callout>
        </Marker>
        <Circle
          center={pin}
          radius={1000}
          strokeWidth={2}
          strokeColor="cornflowerblue"
        />
        <Marker pinColor="blue" coordinate={shops.hospital}>
          <Callout>
            <Text>Location: Shatin 302</Text>
            <Text>Service: photo printing</Text>
            <Text>Tel: 54676789</Text>
          </Callout>
        </Marker>
        <Marker pinColor="blue" coordinate={shops.shatin}>
          <Callout>
            <Text>shop2</Text>
          </Callout>
        </Marker>
        <Marker pinColor="blue" coordinate={shops.hengon}>
          <Callout>
            <Text>shop3</Text>
          </Callout>
        </Marker>
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  searchContainer: {
    top: 1,
    position: "absolute",
    width: "95%",
    zIndex: 1,
    backgroundColor: "white",
    shadowColor: "balck",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.6,
    shadowRadius: 4,
    elevation: 4,
    padding: 5,
    borderRadius: 8,
  },
  input: {
    borderColor: colors.light,
    borderWidth: 1,
  },
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});
export default AppMap;
