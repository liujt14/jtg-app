import React from "react";
import { useFormikContext } from "formik";

import AppPicker from "../lists/AppPicker";
import ErrorMessage from "./ErrorMessage";
import { number } from "yup";

function AppFormPicker({
  items,
  numberOfColumns,
  name,
  width,
  PickerItemComponent,
  placeholder,
}) {
  const { errors, setFieldValue, touched, values } = useFormikContext();
  return (
    <>
      <AppPicker
        items={items}
        numberOfColumns={numberOfColumns}
        PickerItemComponent={PickerItemComponent}
        onSelectItem={(item) => setFieldValue(name, item)}
        placeholder={placeholder}
        selectedItem={values[name]}
        width={width}
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
}

export default AppFormPicker;
