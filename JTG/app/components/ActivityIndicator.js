import React from "react";
import LottieView from "lottie-react-native";
import { View, StyleSheet } from "react-native";

function ActivityIndicator({ visible = false }) {
  if (!visible) return null;
  return (
    <View style={styles.layout}>
      <LottieView
        autoPlay
        loop
        source={require("../assets/animations/layer.json")}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  layout: {
    position: "absolute",
    backgroundColor: "white",
    width: "100%",
    height: "100%",
    opacity: 0.8,
    zIndex: 1,
  },
});

export default ActivityIndicator;
