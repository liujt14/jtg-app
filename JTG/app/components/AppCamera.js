import { Camera, CameraType } from "expo-camera";
import { useState, useRef } from "react";
import { StyleSheet, View } from "react-native";
import * as FaceDetector from "expo-face-detector";
import * as MediaLibrary from "expo-media-library";

import CameraPreview from "./CameraPreview";
import { AppButton } from "./lists";

const AppCamera = () => {
  const [type, setType] = useState(CameraType.back);

  const [previewVisible, setPreviewVisible] = useState(false);
  const [capturedImage, setCapturedImage] = useState(null);

  const cameraRef = useRef(null);

  function toggleCameraType() {
    setType((current) =>
      current === CameraType.back ? CameraType.front : CameraType.back
    );
  }
  const takePicture = async () => {
    const photo = await cameraRef.current.takePictureAsync();
    setPreviewVisible(true);
    setCapturedImage(photo);
  };
  const __startCamera = async () => {
    const { status } = await Camera.requestCameraPermissionsAsync();
    if (status === "granted") {
      // start the camera
    } else {
      Alert.alert("Access denied");
    }
  };
  const reTake = () => {
    setCapturedImage(null);
    setPreviewVisible(false);
    __startCamera();
  };
  const savePhoto = async () => {
    console.log(capturedImage.uri);
    MediaLibrary.saveToLibraryAsync(capturedImage.uri);
  };

  const handleFacesDetected = ({ faces }) => {};

  return (
    <View style={styles.container}>
      {previewVisible && capturedImage ? (
        <CameraPreview
          photo={capturedImage}
          reTake={reTake}
          savePhoto={savePhoto}
        />
      ) : (
        <Camera
          style={styles.camera}
          type={type}
          ref={cameraRef}
          onFacesDetected={handleFacesDetected}
          faceDetectorSettings={{
            mode: FaceDetector.FaceDetectorMode.fast,
            detectLandmarks: FaceDetector.FaceDetectorLandmarks.none,
            runClassifications: FaceDetector.FaceDetectorClassifications.none,
            minDetectionInterval: 100,
            tracking: true,
          }}
        >
          <View style={styles.buttonContainer}>
            <AppButton title="Flip Camera" onPress={toggleCameraType} />
            <AppButton title="Take Picture" onPress={takePicture} />
          </View>
        </Camera>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  camera: {
    flex: 1,
    width: "100%",
    height: "100%",
  },
  buttonContainer: {
    opacity: 0.8,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    flex: 1,
    width: "50%",
    padding: 10,
    justifyContent: "space-between",
    margin: 10,
  },
});

export default AppCamera;
