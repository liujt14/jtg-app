import react from "react";
import { View, ImageBackground, StyleSheet } from "react-native";
import { AppButton } from "./lists";

const CameraPreview = ({ photo, reTake, savePhoto }) => {
  console.log("sdsfds", photo);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={{ uri: photo && photo.uri }}
        style={styles.image}
      />
      <View style={styles.buttonContainer}>
        <AppButton title="Re-Take" onPress={reTake} />
        <AppButton title="Save-Photo" onPress={savePhoto} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    flex: 1,
    width: "100%",
    height: "100%",
  },
  buttonContainer: {
    opacity: 0.8,
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    flex: 1,
    width: "50%",
    padding: 10,
    justifyContent: "space-between",
    margin: 10,
  },
  image: {
    flex: 1,
  },
});

export default CameraPreview;
