import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
//import { Colors } from "../config/colors";
function Card({ title, subTitle, imageUrl, onPress }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.card}>
        <Image style={styles.image} source={{ url: imageUrl }} />
        <View style={styles.detailContainer}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subTitle}>{subTitle}</Text>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  card: {
    borderRadius: 20,
    backgroundColor: "#fff",
    marginBottom: 10,
  },
  image: {
    width: "100%",
    height: 180,
  },
  detailContainer: {
    padding: 20,
    overflow: "hidden",
  },
  subTitle: {
    marginTop: 10,
    fontStyle: "italic",
    fontWeight: "bold",
    color: "green",
  },
  title: {
    marginBottom: 10,
  },
});

export default Card;
