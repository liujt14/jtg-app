import "react-native-gesture-handler";
import react, { useEffect, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";

import AuthNavigator from "./app/navigation/AuthNavigator";
import NavigationTheme from "./app/navigation/NavigationTheme";
import AuthContext from "./app/auth/context";
import authStorage from "./app/auth/storage";
import AppNavigatorFive from "./app/navigation/AppNavigatorFive";

export default function App() {
  console.log("app excuted");

  const [user, setUser] = useState();

  const restoreUser = async () => {
    const user = await authStorage.getUser();
    if (user) setUser(user);
  };

  useEffect(() => {
    restoreUser();
  }, []);

  return (
    <AuthContext.Provider value={{ user, setUser }}>
      <NavigationContainer theme={NavigationTheme}>
        {user ? <AppNavigatorFive /> : <AuthNavigator />}
      </NavigationContainer>
    </AuthContext.Provider>
  );
}
