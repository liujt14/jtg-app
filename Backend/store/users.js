const users = [
  {
    id: 1,
    name: "Mosh",
    email: "mosh@domain.com",
    password: "12345",
  },
  {
    id: 2,
    name: "John",
    email: "john@domain.com",
    password: "12345",
  },
  {
    id: 3,
    name: "liujt",
    email: "liujt14@jtg.com",
    password: "12345",
  },
  {
    id: 4,
    name: "yanqing",
    email: "yanqing@jtg.com",
    password: "12345",
  },
  {
    id: 5,
    name: "songrui",
    email: "songrui@jtg.com",
    password: "12345",
  },
];

const getUsers = () => users;

const getUserById = (id) => users.find((user) => user.id === id);

const getUserByEmail = (email) => users.find((user) => user.email === email);

const addUser = (user) => {
  user.id = users.length + 1;
  users.push(user);
};

module.exports = {
  getUsers,
  getUserByEmail,
  getUserById,
  addUser,
};
